#!/usr/bin/env python

import os
import pathlib
import sys

import fire


SCRIPT = """
#!/bin/bash

set -o errexit
%s

IMAGE_NAME="%s"
docker pull "$IMAGE_NAME" >/dev/null

volumes=$(
    docker inspect --format \
        '{{ range $volume, $config := .Config.Volumes }}{{ $volume }} {{ end }}' \
        "$IMAGE_NAME"
)
volume_flags=
for volume in $volumes; do
    volume_flags="$volume_flags --volume $HOME$volume:$volume"
done

port_flags=$(
    docker inspect --format \
        '{{ range $port, $config := .Config.ExposedPorts }}--publish {{ index (split $port "/") 0 }}:{{ $port }} {{ end }}' \
        "$IMAGE_NAME"
)

if [[ "$1" == "--docksi-dry-run" ]]; then
    echo "Volume flags: $volume_flags"
    echo "Port flags: $port_flags"
else
    docker run --rm --user "$(id -u):$(id -g)" --interactive --tty $volume_flags $port_flags \
        "$IMAGE_NAME" $*
fi
""".strip()

# Expecting that ~/.local/docksi-bin exists already on the host,
# and is mounted into the docksi container at /.local/docksi-bin

BIN_PATH = pathlib.Path("/.local/docksi-bin")


class Docksi:
    """
    Lightweight wrapper for installing docker-based CLI images and letting them
    specify extra arguments to docker run, such as mounts and port forwards.
    """

    def install(self, image_name, debug=False):
        """
        Install an image. The name of the image (without any prefixes) will be
        the name of the installed command.
        """

        image_name_and_tag = image_name.split("/")[-1]
        command_name = image_name_and_tag.split(":")[0]

        command_path = BIN_PATH / command_name

        if command_path.exists():
            print(f"{command_path} already exists, deleting it and re-installing.")
            command_path.unlink()

        with open(command_path, "w") as command_file:
            debug_flag = "set -o xtrace" if debug else ""
            command_file.write(SCRIPT % (debug_flag, image_name))

        command_path.chmod(0o755)

        print(command_name, "installed sucessfully!")

    def uninstall(self, command_name):
        """
        Uninstall an image. Deletes the wrapper script, but does not delete
        any pulled docker images.
        """

        command_path = BIN_PATH / command_name

        if not command_path.exists():
            print(f"Fatal: {command_name} is not installed.")
            sys.exit(1)

        command_path.unlink()

        print(command_name, "has been uninstalled.")

    def list(self):
        """ List commands that were installed with Docksi. """

        print("Docksi installed commands:")
        for command in os.listdir(BIN_PATH):
            print(f"  {command}")


if __name__ == "__main__":
    fire.Fire(Docksi)
